



### Swagger Documentation

#### Install Swagger Library

```
go get -u github.com/swaggo/swag/cmd/swag
```

#### Generate Swagger Documentation

```
~/go/bin/Swag init
```


### Fix Problem for Authenticate

```
I have added as json file:
"consumes": [
"application/x-www-form-urlencoded",
"multipart/form-data"
]
and this as yaml file:
consumes:
- application/x-www-form-urlencoded
- multipart/form-data
```

