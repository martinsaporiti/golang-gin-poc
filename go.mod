module gitlab.com/martinsaporiti/golang-gin-poc

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/go-openapi/spec v0.19.7 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.5
	github.com/urfave/cli v1.22.4 // indirect
	gitlab.com/pragmaticreviews/golang-gin-poc v0.0.0-20200217232121-d0a67e33c0bd
	golang.org/x/crypto v0.0.0-20200414173820-0848c9571904 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
	golang.org/x/tools v0.0.0-20200416214402-fc959738d646 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
