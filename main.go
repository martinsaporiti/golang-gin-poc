// Package main ...
package main

import (
	"io"
	"os"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	swaggerFiles "github.com/swaggo/gin-swagger/swaggerFiles"

	"gitlab.com/martinsaporiti/golang-gin-poc/api"
	"gitlab.com/martinsaporiti/golang-gin-poc/controller"
	"gitlab.com/martinsaporiti/golang-gin-poc/docs"
	_ "gitlab.com/martinsaporiti/golang-gin-poc/docs"

	"gitlab.com/martinsaporiti/golang-gin-poc/middlewares"
	"gitlab.com/martinsaporiti/golang-gin-poc/repository"
	"gitlab.com/martinsaporiti/golang-gin-poc/service"
)

var (
	videoRepository repository.VideoRepository = repository.NewVideoRepository()
	videoService    service.VideoService       = service.New(videoRepository)
	videoController controller.VideoController = controller.New(videoService)
	loginController controller.LoginController = controller.NewLoginController(loginService, jwtService)
	loginService    service.LoginService       = service.NewLoginService()
	jwtService      service.JWTService         = service.NewJWTService()
)

// Configura la salida por defecto de gin
func setUpLogOutput() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

// @securityDefinitions.apikey bearerAuth
// @in header
// @name Authorization
func main() {

	// Se configura la salida del loggin de gin:
	setUpLogOutput()

	// Swagger 2.0 Meta Information
	docs.SwaggerInfo.Title = "Sapo Reviews - Video API"
	docs.SwaggerInfo.Description = "Sapo Reviews - Youtube Video API."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:3000"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"http"}

	defer videoRepository.CloseDB()

	server := gin.New()

	server.Static("/css", "./templates/css")
	server.LoadHTMLGlob("templates/*.html")

	videoAPI := api.NewVideoAPI(loginController, videoController)
	apiRoutes := server.Group(docs.SwaggerInfo.BasePath)

	{

		login := apiRoutes.Group("/auth")
		{
			login.POST("/token", videoAPI.Authenticate)
		}

		videos := apiRoutes.Group("/videos", middlewares.AuthorizeJWT())
		{
			videos.GET("", videoAPI.GetVideos)
			videos.POST("", videoAPI.CreateVideo)
			videos.PUT(":id", videoAPI.UpdateVideo)
			videos.DELETE(":id", videoAPI.DeleteVideo)
		}

	}

	viewRoutes := server.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}

	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// server.Use(gin.Recovery(), middlewares.Logger(), middlewares.BasicAuth())
	server.Use(gin.Recovery(), middlewares.Logger())

	server.Run(":3000")
}
